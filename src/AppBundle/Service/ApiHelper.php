<?php
/**
 * Created by PhpStorm.
 * User: Proweb
 * Date: 06/03/2018
 * Time: 14:41
 */

namespace AppBundle\Service;


class ApiHelper
{
    /**
     * @var string
     */
    private $url_api = 'http://178.33.165.135/api/1.0/';
    /**
     * @var int
     */
    private $default_page_size = 99999;

    /**
     * Get data from API given a URL suffix
     *
     * @param string $suffix
     * @param boolean $assoc
     *
     * @return mixed
     */
    public function getApi($suffix, $assoc = false)
    {
        if (substr($suffix, 0) === '/') $suffix = substr($suffix, 1);
        // TODO It's best practice analysing 'next' attribute which contains the URL to the next group of items paginated, when they are more than 50 items
        $url = $this->url_api . $suffix . '?page_size=' . $this->default_page_size;
        $response = file_get_contents($url);
        return json_decode($response, $assoc);
    }

    /**
     * Get tires from API
     *
     * @return mixed
     */
    public function getJsonTires()
    {
        return $this->getApi('tires/');
    }

    /**
     * Get ticks from API
     *
     * @return mixed
     */
    public function getJsonTicks()
    {
        return $this->getApi('ticks/');
    }

    /**
     * Get clients from API
     *
     * @return mixed
     */
    public function getJsonClients()
    {
        return $this->getApi('clients/');
    }

    /**
     * Get station users from API
     *
     * @return mixed
     */
    public function getJsonStationUsers()
    {
        return $this->getApi('stationsUsers/');
    }
}