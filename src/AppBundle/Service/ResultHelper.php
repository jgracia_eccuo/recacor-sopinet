<?php
/**
 * Created by PhpStorm.
 * User: Proweb
 * Date: 06/03/2018
 * Time: 14:55
 */

namespace AppBundle\Service;


use AppBundle\ApiModel\Tick;
use AppBundle\ApiModel\Tire;
use AppBundle\Model\GraphicData;

// TODO Mejorar nombres de las funciones a inglés
class ResultHelper
{
    /**
     * @var ApiHelper
     */
    protected $apiHelper;

    public function __construct(ApiHelper $apiHelper)
    {
        $this->apiHelper = $apiHelper;
    }

    /**
     * Get an array of tires from API
     *
     * @param integer $year
     *
     * @return array
     */
    private function getTires($year)
    {
        $json_tires = $this->apiHelper->getJsonTires();
        $tires = array();
        foreach ($json_tires->results as $result) {
            $tire = new Tire($result);
            if ($tire->isCreatedAtYear($year)) {
                $tires[] = $tire;
            }
        }

        return $tires;
    }

    /**
     * Get the number of tires from API
     *
     * @return integer
     */
    private function getCountTires()
    {
        $json_tires = $this->apiHelper->getJsonTires();
        return $json_tires->count;
    }

    /**
     * Get an array of total tires by month
     *
     * @param integer $year
     *
     * @return array
     */
    private function getTotalTires($year)
    {
        $total_per_months = [
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
            6 => 0,
            7 => 0,
            8 => 0,
            9 => 0,
            10 => 0,
            11 => 0,
            12 => 0,
        ];
        foreach ($this->getTires($year) as $tire) {
            $total_per_months[$tire->getMonth()] += 1;
        }
        return $total_per_months;
    }


    /**
     * Get an array of ticks from API
     *
     * @param integer $year
     *
     * @return array
     */
    private function getTicks($year)
    {
        $json_tires = $this->apiHelper->getJsonTicks();
        $ticks = array();
        foreach ($json_tires->results as $result) {
            $tick = new Tick($result);
            if ($tick->isCreatedAtYear($year)) {
                $ticks[] = $tick;
            }
        }

        return $ticks;
    }

    /**
     * Get an array of total 'somethings' by jobs
     *
     * @param integer $year
     *
     * @return array
     */
    private function getTotalPuestos($year)
    {
        // TODO It's best practice getting IDs from API
        $puestos = [
            'repasado' => [
                'name' => 'Repasado',
                'total' => 0,
            ],
            'insinicial' => [
                'name' => 'Inspección inicial',
                'total' => 0,
            ],
            'raspado' => [
                'name' => 'Raspado',
                'total' => 0,
            ],
            'repaencem' => [
                'name' => 'Reparación-encementado',
                'total' => 0,
            ],
            'posgoma' => [
                'name' => 'Postura goma',
                'total' => 0,
            ],
            'vulcanizado' => [
                'name' => 'Vulcanizado',
                'total' => 0,
            ],
            'insfinal' => [
                'name' => 'Inspección final',
                'total' => 0,
            ],
        ];

        $ticks = $this->getTicks($year);
        foreach ($puestos as $key => $puesto) {
            foreach ($ticks as $tick) {
                if ($tick->isStationName($puesto['name'])) {
                    $puestos[$key]['total'] += 1;
                }
            }
        }

        return $puestos;
    }

    /**
     * Get an array of total 'somethings' by employees
     *
     * @param integer $year
     *
     * @return array
     */
    private function getTotalEscaneos($year)
    {
        $users = [
            'admin' => [
                'name' => 'admin3',
                'total' => 0,
            ],
            'jesus' => [
                'name' => 'Jesus',
                'total' => 0,
            ],
            'antonio' => [
                'name' => 'Antonio',
                'total' => 0,
            ],
            'pepe' => [
                'name' => 'Pepe',
                'total' => 0,
            ],
            'admin2' => [
                'name' => 'admin2',
                'total' => 0,
            ],
            'jose' => [
                'name' => 'Jose',
                'total' => 0,
            ],
        ];

        $ticks = $this->getTicks($year);
        foreach ($users as $key => $user) {
            foreach ($ticks as $tick) {
                if ($tick->isStationUsername($user['name'])) {
                    $users[$key]['total'] += 1;
                }
            }
        }

        return $users;
    }


    /**
     * Get the number of clients from API
     *
     * @return mixed
     */
    private function getCountClients()
    {
        return $this->apiHelper->getJsonClients()->count;
    }


    /**
     * Get a json object with the result attribute of stations users from API
     *
     * @return mixed
     */
    private function getResultsStationsUsers()
    {
        // TODO It's best practice creating a model for Station User
        return $this->apiHelper->getJsonStationUsers()->results;
    }

    /**
     * Get a GraphicData model
     *
     * @param integer $year
     *
     * @return GraphicData
     */
    public function getGraphicData($year)
    {
        if ($year === 'now') {
            $year = intval(date('Y'));
        } else {
            $year = intval($year);
        }

        $tires = $this->getTires($year);
        $months = $this->getTotalTires($year);
        $puestos = $this->getTotalPuestos($year);
        $empleados = $this->getTotalEscaneos($year);
        // TODO Below vars are not used nowhere
        $count_tires = $this->getCountTires();
        $count_clients = $this->getCountClients();

        return new GraphicData($tires, $count_tires, $months, $puestos, $empleados, $count_clients, $year);
    }
}