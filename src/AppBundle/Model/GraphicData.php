<?php
/**
 * Created by PhpStorm.
 * User: Proweb
 * Date: 08/03/2018
 * Time: 16:27
 */

namespace AppBundle\Model;


class GraphicData
{
    /**
     * @var array
     */
    private $tires;
    /**
     * @var integer
     */
    private $count_tires;
    /**
     * @var array
     */
    private $months;
    /**
     * @var array
     */
    private $jobs;
    /**
     * @var array
     */
    private $employees;
    /**
     * @var integer
     */
    private $count_clients;
    /**
     * @var integer
     */
    private $year;

    /**
     * GraphicData constructor.
     * @param array $tires
     * @param integer $count_tires
     * @param array $months
     * @param array $jobs
     * @param array $employees
     * @param integer $count_clients
     * @param integer $year
     */
    public function __construct($tires, $count_tires, $months, $jobs, $employees, $count_clients, $year)
    {
        $this->tires = $tires;
        $this->count_tires = $count_tires;
        $this->months = $months;
        $this->jobs = $jobs;
        $this->employees = $employees;
        $this->count_clients = $count_clients;
        $this->year = $year;
    }

    /**
     * Get tires
     *
     * @return array
     */
    public function getTires()
    {
        return $this->tires;
    }

    /**
     * Get employees
     *
     * @return array
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * Get jobs
     *
     * @return array
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * Get months
     *
     * @return array
     */
    public function getMonths()
    {
        return $this->months;
    }

    /**
     * Get filter year
     *
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

}