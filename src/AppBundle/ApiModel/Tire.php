<?php
/**
 * Created by PhpStorm.
 * User: Proweb
 * Date: 07/03/2018
 * Time: 8:49
 */

namespace AppBundle\ApiModel;


class Tire
{
    /**
     * @var \DateTime
     */
    private $created_at;

    private $garage;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    public function __construct($json_object)
    {
        // TODO: Un ejemplo de fecha json es este 2018-02-15T23:16:44Z. Hay que ver que hacer con Z
        $this->created_at = new \DateTime(substr($json_object->created_at,0,-1));
        $this->garage = $json_object->garage;
        $this->code = $json_object->code;
        $this->name = $json_object->name;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Get garage
     *
     * @return mixed
     */
    public function getGarage()
    {
        return $this->garage;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get month in format n
     *
     * @return string
     */
    public function getMonth()
    {
        return $this->created_at->format('n');
    }

    /**
     * Check if given year is the created at year
     *
     * @param $year
     *
     * @return bool
     */
    public function isCreatedAtYear($year)
    {
        return intval($this->created_at->format('Y')) === $year ? true : false;
    }
}