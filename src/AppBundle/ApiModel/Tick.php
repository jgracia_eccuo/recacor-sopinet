<?php
/**
 * Created by PhpStorm.
 * User: Proweb
 * Date: 07/03/2018
 * Time: 10:26
 */

namespace AppBundle\ApiModel;


class Tick
{
    private $stationUser;
    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * Tick constructor.
     * @param $json_object
     */
    public function __construct($json_object)
    {
        $this->stationUser = $json_object->stationUser;
        // TODO: Un ejemplo de fecha json es este 2018-02-15T23:16:44Z. Hay que ver que hacer con Z
        $this->created_at = new \DateTime(substr($json_object->created_at,0,-1));
    }

    /**
     * Check if given name is the station name
     *
     * @param string $name
     *
     * @return bool
     */
    public function isStationName($name)
    {
        return ($this->stationUser->station->name == $name) ? true : false;
    }

    /**
     * Check if given username is the station username
     *
     * @param string $username
     *
     * @return bool
     */
    public function isStationUsername($username)
    {
        return ($this->stationUser->user->username == $username) ? true : false;
    }

    /**
     * Check if given year is the created at year
     *
     * @param integer $year
     *
     * @return bool
     */
    public function isCreatedAtYear($year)
    {
        return intval($this->created_at->format('Y')) === $year ? true : false;
    }
}