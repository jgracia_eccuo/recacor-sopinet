<?php

namespace AppBundle\Controller;


use AppBundle\Service\ResultHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/{year}", name="homepage", defaults={"year": "now"})
     *
     * @param integer $year
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($year)
    {
        /**
         * @var ResultHelper $result_helper
         */
        $result_helper = $this->get('app.result_helper');
        $graphic_data = $result_helper->getGraphicData($year);
        return $this->render('default/index.html.twig', ['graphic_data' => $graphic_data]);
    }
}
